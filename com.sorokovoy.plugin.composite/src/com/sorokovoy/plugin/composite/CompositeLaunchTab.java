package com.sorokovoy.plugin.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.IDebugModelPresentation;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class CompositeLaunchTab extends AbstractLaunchConfigurationTab {
	public static final String SELECTED_CONFIGURATION_LIST_KEY = "SELECTED_CONFIGURATION_LIST_KEY";
	private TableViewer tableSelectedConf;
	private final IObservableList selectedConfData = new WritableList(
			new ArrayList(), String.class);
	private final ArrayList<String> allConfNames = new ArrayList();
	private TableViewer tableAllConf;
	private final IObservableList allConfData = new WritableList(
			new ArrayList(), String.class);
	private ILaunchConfiguration[] allLaunchConfigurations;

	public void createControl(Composite parent) {
		Composite controls = new Composite(parent, 4);
		controls.setLayout(new FormLayout());

		Label avConfLbl = new Label(controls, 16384);
		avConfLbl.setText("Available launch configurations:");
		FormData avConfLblF = new FormData();
		avConfLblF.left = new FormAttachment(0, 5);
		avConfLblF.right = new FormAttachment(50, -5);
		avConfLblF.top = new FormAttachment(0, 5);
		avConfLbl.setLayoutData(avConfLblF);

		this.tableAllConf = new TableViewer(controls, 2820);
		this.tableAllConf
				.setContentProvider(new ObservableListContentProvider());
		this.tableAllConf.setInput(this.allConfData);

		FormData availableConfigsF = new FormData();
		availableConfigsF.left = new FormAttachment(0, 5);
		availableConfigsF.right = new FormAttachment(50, -5);
		availableConfigsF.top = new FormAttachment(avConfLbl, 5);
		availableConfigsF.bottom = new FormAttachment(100, -5);
		this.tableAllConf.getTable().setLayoutData(availableConfigsF);

		Label selConfLbl = new Label(controls, 16384);
		selConfLbl.setText("Selected launch configurations:");
		FormData selConfLblF = new FormData();
		selConfLblF.left = new FormAttachment(50, 5);
		selConfLblF.right = new FormAttachment(100, -5);
		selConfLblF.top = new FormAttachment(0, 5);
		selConfLbl.setLayoutData(selConfLblF);

		this.tableSelectedConf = new TableViewer(controls, 68354);
		this.tableSelectedConf
				.setContentProvider(new ObservableListContentProvider());
		this.tableSelectedConf.setInput(this.selectedConfData);

		FormData selectedConfigsListF = new FormData();
		selectedConfigsListF.left = new FormAttachment(50, 5);
		selectedConfigsListF.right = new FormAttachment(100, -5);
		selectedConfigsListF.top = new FormAttachment(selConfLbl, 5);
		selectedConfigsListF.bottom = new FormAttachment(100, -5);
		this.tableSelectedConf.getTable().setLayoutData(selectedConfigsListF);

		setIcons(controls);
		addListeners();
		setControl(controls);
	}

	private void setIcons(Composite controls) {
		ILaunchManager launchManager = getLaunchManager();
		final HashMap<String, Image> imgs = new HashMap();
		try {
			allLaunchConfigurations = launchManager.getLaunchConfigurations();
		} catch (CoreException e) {
			ILaunchConfiguration[] launchConfigurations;
			setErrorMessage(e.getMessage());
			return;
		}
		IDebugModelPresentation debugModelPresentation = DebugUITools
				.newDebugModelPresentation();
		for (ILaunchConfiguration conf : allLaunchConfigurations) {
			Image image = debugModelPresentation.getImage(conf);
			imgs.put(conf.getName(), image);
		}
		this.tableAllConf.setLabelProvider(new LabelProvider() {
			public Image getImage(Object element) {
				return (Image) imgs.get(element.toString());
			}
		});
		this.tableSelectedConf.setLabelProvider(new LabelProvider() {
			public Image getImage(Object element) {
				return (Image) imgs.get(element.toString());
			}
		});
	}
	
	private void addListeners() {
		this.tableAllConf
				.addSelectionChangedListener(new ISelectionChangedListener() {
					public void selectionChanged(SelectionChangedEvent event) {
						CompositeLaunchTab.this.addItem();
					}
				});

		this.tableSelectedConf
				.addSelectionChangedListener(new ISelectionChangedListener() {
					public void selectionChanged(SelectionChangedEvent event) {
						CompositeLaunchTab.this.removeItem();
						CompositeLaunchTab.this.updateState();
					}
				});
	}

	private void updateState() {
		updateLaunchConfigurationDialog();
	}
	
	private void addItem() {
		IStructuredSelection sel = (IStructuredSelection) this.tableAllConf.getSelection();
		List sels = sel.toList();
		this.selectedConfData.addAll(sels);
		this.allConfData.removeAll(sels);
		updateState();
	}

	private void removeItem() {
		IStructuredSelection sel = (IStructuredSelection) this.tableSelectedConf.getSelection();
		List sels = sel.toList();
		this.selectedConfData.removeAll(sels);
		this.allConfData.clear();
		for (String c : this.allConfNames) {
			if (!this.selectedConfData.contains(c)) {
				this.allConfData.add(c);
			}
		}
		updateState();
	}

	public void initializeFrom(ILaunchConfiguration configuration) {
		ILaunchManager launchManager = getLaunchManager();
		try {
			allLaunchConfigurations = launchManager.getLaunchConfigurations();
		} catch (CoreException e) {
			setErrorMessage(e.getMessage());
			return;
		}

		List<String> selectedList = new ArrayList(); 
		try {
			selectedList = configuration.getAttribute(CompositeLaunchTab.SELECTED_CONFIGURATION_LIST_KEY, Collections.EMPTY_LIST);
		} catch (CoreException e) {
			setErrorMessage(e.getMessage());
		}

		this.allConfNames.clear();
		this.allConfData.clear();
		for (ILaunchConfiguration conf : allLaunchConfigurations) {
			String configurationName = conf.getName();
			if (!configurationName.equals(configuration.getName())) {
				this.allConfNames.add(configurationName);
				if (!selectedList.contains(configurationName)) {
					this.allConfData.add(configurationName);
				}
			}
		}
		this.selectedConfData.clear();
		for (String s : selectedList) {
			this.selectedConfData.add(s);
			if (!this.allConfNames.contains(s)) {
				setErrorMessage("Configuration [" + s + "] is undefined");
			}
		}
		updateState();
	}
	
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		configuration.setAttribute(CompositeLaunchTab.SELECTED_CONFIGURATION_LIST_KEY, new ArrayList(this.selectedConfData));
	}

	public boolean isValid(ILaunchConfiguration launchConfig) {
		for (Object s : this.selectedConfData) {
			if (!this.allConfNames.contains(s)) {
				setErrorMessage("Configuration [" + s + "] is undefined");
				return false;
			}
		}
		setErrorMessage(null);
		return super.isValid(launchConfig);
	}


	public String getName() {
		return "Composite";
	}

	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {		
	}
}
