package com.sorokovoy.plugin.composite;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;

public class CompositeLaunchConfigurationDelegate
  implements ILaunchConfigurationDelegate
{
  public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor)
    throws CoreException
  {
    List<String> configurationNameList = configuration.getAttribute(CompositeLaunchTab.SELECTED_CONFIGURATION_LIST_KEY, Collections.EMPTY_LIST);
    
    ILaunchManager launchManager = DebugPlugin.getDefault().getLaunchManager();
    ILaunchConfiguration[] allLaunchConfigurations = launchManager.getLaunchConfigurations();
    HashMap<String, ILaunchConfiguration> launchConfigurations = new HashMap();
    for (ILaunchConfiguration c : allLaunchConfigurations) {
      launchConfigurations.put(c.getName(), c);
    }
    for (String name : configurationNameList) {
      if (launchConfigurations.containsKey(name)) {
        ((ILaunchConfiguration)launchConfigurations.get(name)).launch(mode, null);
      }
    }
  }
}